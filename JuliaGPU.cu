#include <iostream>
#include <book.h>
#include "cpu_bitmap.h"
#include "Julia.h"



int main()
{
    CPUBitmap vBitmap( Dim, Dim );

    unsigned char * dev_Bitmap;

    HANDLE_ERROR(cudaMalloc((void**)&dev_Bitmap,vBitmap.image_size()));

    dim3
    grid(Dim,Dim);
    kernel<<<grid,1>>>( dev_Bitmap );
    HANDLE_ERROR( cudaMemcpy( vBitmap.get_ptr(), dev_Bitmap,vBitmap.image_size(),cudaMemcpyDeviceToHost ) );
    vBitmap.display_and_exit();
    HANDLE_ERROR( cudaFree( dev_Bitmap ) );



    return 0;
}