
---------------------------------------------------------------------------------

Short program, that computes Julia set on matrix 500x500

This files must be run on device containing CUDA graphic cards and  NVCC compilator installed

Also, you need to ensure that you have version of GLUT installed on your device

It was tested on NVIDIA GeForce 740M


To compile & run this program, go into JuliaSet folder and write in terminal:

nvcc JuliaGPU.cu -lGLU -lGL -lglut -o Output.o && ./Output.o

---------------------------------------------------------------------------------

Further Improving:
-add clock to measure computation time
-compare times between CPU computation and GPU computation
-add C++ command line parser to load matrix size from command line