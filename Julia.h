#ifndef JULIA_H
#define JULIA_H

/*
----------------------------------------------
                  WARNING!!!

  THIS CODE IS ONLY AVAIBLE FOR CUDA PROGRAM

    DO NOT ATTACH IT TO SIMPLE CPU PROGRAM

----------------------------------------------
 */

#include "book.h"
#include "cpu_bitmap.h"

const int Dim = 500;


//Complex number - struct
struct cuComplex
{
    float vReal;
    float vImag;
    __device__ cuComplex (float a, float b) : vReal(a), vImag(b) {}

    __device__ float Magnitude2(void)
    {
        return vReal * vReal  + vImag * vImag;
    }
    __device__ cuComplex operator*(const cuComplex &tComplex)
    {
        return cuComplex(tComplex.vReal * vReal - vImag * tComplex.vImag,vReal * tComplex.vImag + vImag * tComplex.vReal);
    }
    __device__ cuComplex operator+(const cuComplex & tComplex)
    {
        return cuComplex(vReal + tComplex.vReal,vImag + tComplex.vImag);
    }
};

//Julia function for calcucalting magnitude of point
//DIM - dimension of the calculated bitmap

__device__ int Julia(int x, int y)
{
    const float cScale = 1.5;

    float jX = cScale * (float)(Dim/2 - x)/(Dim/2);
    float jY = cScale * (float)(Dim/2 - y)/(Dim/2);

    cuComplex tCom(-0.8,0.146);
    //calculated complex number
    cuComplex calCom(jX,jY);

    const int cCounter = 200;
    for (int i = 0; i < cCounter; i++)
    {
        calCom = calCom * calCom + tCom;
        //point is not in this space, will be white
        if (calCom.Magnitude2() > 1000)
        {
            return 0;
        }
    }
    
    return 1;
}


__global__ void kernel(unsigned char * ptr)
{
    int x = blockIdx.x;
    int y = blockIdx.y;
    int offset = x + y *gridDim.x;

    //calculating value at the position:
    int juliaValue = Julia(x,y);
    ptr[offset*4 + 0] = 255 * juliaValue;
    ptr[offset*4 + 1] = 0;
    ptr[offset*4 + 2] = 0;
    ptr[offset*4 + 3] = 255;
}


#endif